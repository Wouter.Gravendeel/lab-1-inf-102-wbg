package INF102.lab1.triplicate;

import java.util.List;
import java.util.ArrayList;
import java.util.Collections;

public class MyTriplicate<T> implements ITriplicate<T> {

    @Override
    public T findTriplicate(List<T> list) {
        
        for (int i = 1; i < (list.size()); i++) {

            int occurance = 1;

            for (int j = i+1; j < (list.size()); j++) {

                if (list.get(i).equals(list.get(j)))
                    occurance++;

                    if (occurance ==3)
                        return list.get(i);

            }
            
            
            //if (list.get(i).equals(list.get(i+2)))
            //    return list.get(i);
        }

        return null;
    }
    
}
